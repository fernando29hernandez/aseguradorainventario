const express = require('express');
const bodyParser = require("body-parser");
const app = express();
const sql = require("./db.js");
const fs = require('fs')
const path = require('path')
const pKey = fs.readFileSync(path.join(__dirname, '../../public.key'))
var jwt = require('jsonwebtoken');
function verifyIdToken(idToken) {
    try{
        return jwt.verify(idToken, pKey, { algorithms: ["RS256"] });
    }catch(e)
    {
        return 'error'
    }
    
}
//show single or all vehiculo
/**
 * jwt
 * id
 * placa
 * subastable
 */
//FUNCIONA ARREGLAR PORQUE NO TODOS LOS IF TIENEN SALIDA
// undefined comparar
// query params
app.get('/Vehiculo', (req, res) => {
    let token_jwt = req.query.jwt;
    if (!token_jwt) {
        res.status(403).json({ "response": "Forbidden" });
    } else {
        /**
          *  AQUI VA LA VERIFICACION TOKEN
          */
        let token_payload = verifyIdToken(token_jwt)
        if(token_payload=='error')
        {
            return res.status(403).json({ "response": "Forbidden" });
        }
        let bandera_scope = false
        console.log(token_payload.scope);
        for (let key in token_payload.scope) {
            let value = token_payload.scope[key]; // get the value by key
            if (value == 'vehiculo.get') {
                bandera_scope = true
            }
        }
        if (bandera_scope == true) {
            let id_param = +req.query.id;
            let subastable = (req.query.subastable == 'true');
            let placa = req.query.placa;
            let campos = "id,estado,tipo,marca,linea,modelo,placa,color,arranca,camina,falla_mecanica,garantia_inspeccion,inundado,colision,precio_base,precio_minimo minimo_requerido,numero_motor,numero_chasis,observacion";
            let sql_string = '';
            if (id_param) {
                if (subastable) {
                    if (subastable == true) {
                        if (placa) {
                            sql_string = "SELECT " + campos + "  FROM vehiculo WHERE id=" + id_param + " AND estado=3 AND " + "placa=\'" + placa + "\'";

                        } else {
                            sql_string = "SELECT " + campos + "  FROM vehiculo WHERE id=" + id_param + " AND estado=3";
                        }
                    } else {
                        if (placa) {
                            sql_string = "SELECT " + campos + "  FROM vehiculo WHERE id=" + id_param + " AND " + "placa=\'" + placa + "\'";

                        } else {
                            sql_string = "SELECT " + campos + "  FROM vehiculo WHERE id=" + id_param;
                        }
                    }
                } else {
                    if (placa) {
                        sql_string = "SELECT " + campos + "  FROM vehiculo WHERE id=" + id_param + " AND " + "placa=\'" + placa + "\'";

                    } else {
                        sql_string = "SELECT " + campos + "  FROM vehiculo WHERE id=" + id_param;
                    }
                }
            } else {
                if (subastable) {
                    if (subastable == true) {
                        if (placa) {
                            sql_string = "SELECT " + campos + "  FROM vehiculo WHERE estado=3" + " AND " + "placa=\'" + placa + "\'";

                        } else {
                            sql_string = "SELECT " + campos + "  FROM vehiculo WHERE " + " estado=3";
                        }

                    } else {
                        if (placa) {
                            sql_string = "SELECT " + campos + "  FROM vehiculo WHERE " + "placa=\'" + placa + "\'";

                        } else {
                            sql_string = "SELECT " + campos + "  FROM vehiculo";
                        }

                    }
                } else {
                    if (placa) {
                        sql_string = "SELECT " + campos + "  FROM vehiculo WHERE " + "placa=\'" + placa + "\'";

                    } else {
                        sql_string = "SELECT " + campos + "  FROM vehiculo";
                    }
                }
            }
            let query = sql.query(sql_string, (err, results) => {
                if (err) {
                    res.status(404).json({ "response": "Not Found" });
                } else {
                    if (typeof results.length != undefined) {
                        res.status(200).json({ "response": results });
                    } else {
                        res.status(404).json({ "response": "Not Found" });
                    }
                }
            });
        } else {
            res.status(403).json({ "response": "Forbidden" });
        }
    }
});
app.get('/vehiculo_ajustador/:jwt?', (req, res) => {
    let token_jwt = req.params.jwt;
    if (!token_jwt) {
        res.status(403).json({ "response": "Forbidden" });
    } else {
        /**
          *  AQUI VA LA VERIFICACION TOKEN
          */
        let id_param = req.query.id;
        let sql_string = '';
        if (id_param) {
            sql_string = "SELECT * FROM vehiculo WHERE ajustador=" + id_param;
            let query = sql.query(sql_string, (err, results) => {
                if (err) {
                    res.status(404).json({ "response": "Not Found" });
                } else {
                    if (typeof results.length != undefined) {
                        res.status(200).json({ "response": results });
                    } else {
                        res.status(404).json({ "response": "Not Found" });
                    }
                }
            });
        } else {
            res.status(404).json({ "response": "Not Found" });
        }
    }
});

// update del monto de un vehiculo
// NO LO HE PROBADO
app.put('/Vehiculo', (req, res) => {
    let token_jwt = req.body.jwt;
    if (!token_jwt) {
        res.status(403).json({ "response": "Forbidden" });
    } else {
        /**
          *  AQUI VA LA VERIFICACION TOKEN
          */
        let token_payload = verifyIdToken(token_jwt)
        if(token_payload=='error')
        {
            return res.status(403).json({ "response": "Forbidden" });
        }
        let bandera_scope = false
        console.log(token_payload.scope);
        for (let key in token_payload.scope) {
            let value = token_payload.scope[key]; // get the value by key
            if (value == 'vehiculo.put') {
                bandera_scope = true
            }
        }
        if (bandera_scope == true) {
            let id_param = +req.body.id;
            let estado = +req.body.estado;
            let afiliado = +req.body.afiliado_adjudicado;
            let valor = +req.body.valor_adjudicacion;
            if (!id_param) {
                res.status(404).json({ "response": "Not found" });
            } else {
                let sql_string = "";
                if (estado && afiliado && valor) {
                    sql_string = "UPDATE vehiculo SET estado=" + estado + " WHERE id=" + id_param;
                    let query = sql.query(sql_string, (err, results) => {
                        if (err) {
                            res.status(404).json({ "response": "Not found" });
                        } else {
                        }
                    });
                    sql_string = "INSERT INTO puja SET ?";
                    let puja_cuerpo =
                    {
                        id_afiliado: afiliado,
                        id_vehiculo: id_param,
                        monto: valor
                    };
                    let query_puja = sql.query(sql_string, puja_cuerpo, (err, results) => {
                        if (err) {
                            res.status(404).json({ "response": "Not Found" });
                        } else {
                            res.status(201).json({ "respuesta": true });
                        }
                    });
                } else if (estado && !afiliado && !valor) {
                    sql_string = "UPDATE vehiculo SET estado=" + estado + " WHERE id=" + id_param;
                    let query = sql.query(sql_string, (err, results) => {
                        if (err) {
                            res.status(404).json({ "response": "Not found" });
                        } else {
                            res.status(201).json({ "respuesta": true });
                        }
                    });
                } else if (!estado && afiliado && valor) {
                    sql_string = "INSERT INTO puja SET ?";
                    let puja_cuerpo =
                    {
                        id_afiliado: afiliado,
                        id_vehiculo: id_param,
                        monto: valor
                    };
                    let query_puja = sql.query(sql_string, puja_cuerpo, (err, results) => {
                        if (err) {
                            res.status(404).json({ "response": "Not Found" });
                        } else {
                            res.status(201).json({ "respuesta": true });
                        }
                    });
                } else if ((!estado && !afiliado && valor) || (!estado && afiliado && !valor)) {
                    res.status(406).json({ "response": "Not Acceptable" });
                } else if ((estado && !afiliado && valor) || (estado && afiliado && !valor)) {
                    res.status(406).json({ "response": "Not Acceptable" });
                } else {
                    res.status(406).json({ "response": "Not Acceptable" });
                }

            }
        } else {
            res.status(403).json({ "response": "Forbidden" });
        }
    }
});
// update del monto de un vehiculo
// NO LO HE PROBADO
app.put('/Vehiculo_ajustador/:jwt/:id', (req, res) => {
    let token_jwt = req.params.jwt;
    if (!token_jwt) {
        res.status(403).json({ "response": "Forbidden" });
    } else {
        /**
          *  AQUI VA LA VERIFICACION TOKEN
          */
        let id_param = req.params.id;
        let estado = req.query.estado;
        if (!id_param) {
            res.status(404).json({ "response": "Not found" });
        } else {
            let sql_string = "";
            if (estado) {
                sql_string = "UPDATE vehiculo SET estado=" + estado + " WHERE id=" + id_param;
                let query = sql.query(sql_string, (err, results) => {
                    if (err) {
                        res.status(404).json({ "response": "Not found" });
                    } else {
                        res.status(200).json({ "response": "Created" });
                    }
                });
            } else {
                res.status(406).json({ "response": "Not Acceptable" });
            }
        }
    }
});


//add new vehiculo 
// PROBADA
app.post('/Vehiculo/:jwt?', (req, res) => {
    let jwt = req.params.jwt;
    let sql_string = "INSERT INTO vehiculo SET ?";
    if (!jwt) {
        res.status(403).json({ "response": "Forbidden" });
    }
    else {
        let data = {
            estado: req.body.estado,
            tipo: req.body.tipo,
            marca: req.body.marca,
            linea: req.body.linea,
            modelo: req.body.modelo,
            placa: req.body.placa,
            color: req.body.color,
            numero_chasis: req.body.numero_chasis,
            numero_motor: req.body.numero_motor,
            ajustador: req.body.ajustador,
            precio_base: req.body.precio_base,
            precio_minimo: req.body.precio_minimo,
            arranca: req.body.arranca,
            camina: req.body.camina,
            falla_mecanica: req.body.falla_mecanica,
            garantia_inspeccion: req.body.garantia_inspeccion,
            inundado: req.body.inundado,
            colision: req.body.colision,
            observacion: req.body.observacion
        };
        let query = sql.query(sql_string, data, (err, results) => {
            if (err) {
                res.status(404).json({ "response": "Not Found" });
            } else {
                res.status(201).json({ "response": "Created" });
            }
        });
    }
});
// PROBADO 
// [{id,no se para que nombre ,true si estado == 3}]
app.get('/Estado', (req, res) => {
    let jwt = req.query.jwt;
    if (!jwt) {
        res.status(403).json({ "response": "Forbidden" });
    } else {
        let token_payload = verifyIdToken(jwt)
        if(token_payload=='error')
        {
            return res.status(403).json({ "response": "Forbidden" });
        }
        let bandera_scope = false
        console.log(token_payload.scope);
        for (let key in token_payload.scope) {
            let value = token_payload.scope[key]; // get the value by key
            if (value == 'estado.get') {
                bandera_scope = true
            }
        }
        if (bandera_scope == true) {
            let id = +req.query.id;
            let sql_string = '';
            if (id) {
                sql_string = "SELECT id,descripcion nombre,(id=3)=true subastable FROM estado WHERE id=" + id;
            } else {
                sql_string = "SELECT id,descripcion nombre,(id=3)=true subastable FROM estado";
            }
            let query = sql.query(sql_string, (err, results) => {
                if (err) {
                    res.status(404).json({ "response": "Not Found" });
                } else {
                    res.status(200).json({ "response": results });
                }
            });
        } else {
            res.status(403).json({ "response": "Forbidden" });
        }
    }
});
app.get('/puja', (req, res) => {
    const tokenJwt = req.query.jwt
    if (!tokenJwt) {
      res.status(403).json({ response: 'Forbidden' })
    } else {
      /**
            *  AQUI VA LA VERIFICACION TOKEN
            */
      const idParam = req.query.id
      let sqlString = ''
      if (idParam) {
        sqlString = 'SELECT * FROM puja WHERE id_vehiculo=' + idParam
        sql.query(sqlString, (err, results) => {
          if (err) {
            res.status(404).json({ response: 'Not Found' })
          } else {
            if (typeof results.length != undefined) {
              res.status(200).json({ response: results })
            } else {
              res.status(404).json({ response: [] })
            }
          }
        })
      } else {
        res.status(404).json({ response: 'Not Found' })
      }
    }
})
module.exports = app;
