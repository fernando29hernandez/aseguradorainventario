const express = require('express');
const bodyParser = require("body-parser");
const app = express();
const sql = require("./db.js");
const fs = require('fs')
const path = require('path')
const pKey = fs.readFileSync(path.join(__dirname, '../../public.key'))
var jwt = require('jsonwebtoken');
function verifyIdToken(idToken) {
    try{
        return jwt.verify(idToken, pKey, { algorithms: ["RS256"] });
    }catch(e)
    {
        return 'error'
    }
    
}
//add new foto 
// PROBADA
app.post('/Foto/:jwt', (req, res) => {
    let jwt = req.params.jwt;
    let sql_string = "INSERT INTO foto_vehiculo SET ?";
    if (!jwt) {
        res.status(403).json({ "response": "Forbidden" });
    }
    else {
        let data = {
            url: req.body.url,
            externa: req.body.externa,
            fk_vehiculo: req.body.id_vehiculo,
        };
        let query = sql.query(sql_string, data, (err, results) => {
            if (err) {
                res.status(404).json({ "response": "Not Found" });
            } else {
                res.status(201).json({ "response": "Created" });
            }
        });
    }
});
// SI viene el externo true solo externas si viene false internas 
// [{id,url}]
app.get('/Foto', (req, res) => {
    let jwt = req.query.jwt;
    if (!jwt) {
        res.status(403).json({ "response": "Forbidden" });
    } else {
        /**
         * VERIFICACION DE JWT
         */
        let token_payload = verifyIdToken(jwt)
        if(token_payload=='error')
        {
            return res.status(403).json({ "response": "Forbidden" });
        }
        let bandera_scope = false
        console.log(token_payload.scope);
        for (let key in token_payload.scope) {
            let value = token_payload.scope[key]; // get the value by key
            if (value == 'foto.get') {
                bandera_scope = true
            }
        }
        if (bandera_scope == true) {
            let id = +req.query.id;
            let sql_string = "";
            if (id) {
                let externa = (req.query.externa == 'true');
                if (externa) {
                    if (externa == false) {
                        sql_string += "SELECT id,url,fk_vehiculo id_vehiculo FROM foto_vehiculo WHERE fk_vehiculo=" + id;

                    } else {
                        sql_string += "SELECT id,url,fk_vehiculo id_vehiculo FROM foto_vehiculo WHERE fk_vehiculo=" + id + " AND externa=true";
                    }
                } else {
                    sql_string += "SELECT id,url,fk_vehiculo id_vehiculo FROM foto_vehiculo WHERE fk_vehiculo=" + id;
                }
            } else {
                let externa = (req.query.externa == true);
                if (externa) {
                    if (externa == false) {
                        sql_string += "SELECT id,url,fk_vehiculo id_vehiculo FROM foto_vehiculo";
                    } else {
                        sql_string += "SELECT id,url,fk_vehiculo id_vehiculo FROM foto_vehiculo WHERE externa=true";
                    }
                } else {
                    sql_string += "SELECT id,url,fk_vehiculo id_vehiculo FROM foto_vehiculo";
                }
            }
            sql.query(sql_string, (err, results) => {
                if (err) {
                    res.status(404).json({ "response": "Not Found" });
                } else {
                    if (typeof results.lenght != undefined) {
                        res.status(200).json({ "response": results });
                    } else {
                        res.status(200).json({ "response": [] });
                    }

                }
            });
        }
        else {
            res.status(403).json({ "response": "Forbidden" });
        }

    }
});
app.get('/foto_completo/:jwt/:id', (req, res) => {
    let jwt = req.params.jwt;
    if (!jwt) {
        res.status(403).json({ "response": "Forbidden" });
    } else {
        /**
         * VERIFICACION DE JWT
         */
        let sql_string = "SELECT * FROM foto_vehiculo WHERE fk_vehiculo=" + req.params.id;
        let query = sql.query(sql_string, (err, results) => {
            if (err) {
                res.status(404).json({ "response": "Not Found" });
            } else {
                res.status(200).json({ "response": results });
            }
        });

    }
});

app.put('/estado_foto/:jwt/:id', (req, res) => {
    let jwt = req.params.jwt;
    if (!jwt) {
        res.status(403).json({ "response": "Forbidden" });
    } else {
        /**
         * VERIFICACION DE JWT
         */
        let sql_string = "";
        let cambio = req.query.externa;
        let id = +req.params.id;
        sql_string = "UPDATE foto_vehiculo SET externa=" + cambio + " WHERE id=" + id;
        let query = sql.query(sql_string, (err, results) => {
            if (err) {
                res.status(404).json({ "response": "Not found" });
            } else {
                res.status(200).json({ "response": "Modified" });
            }
        });
    }
});
module.exports = app;
