const express = require('express');
const bodyParser = require("body-parser");
const app = express();
const sql = require("./db.js");
//show single or all ajustador
app.get('/ajustador/:jwt', (req, res) => {
    let token_jwt = req.params.jwt;
    if (!token_jwt) {
        res.status(403).json({ "response": "Forbidden" });
    } else {
        /**
          *  AQUI VA LA VERIFICACION TOKEN
          */
        let id_param = req.query.id;
        let sql_string = '';
        if (id_param) {
            sql_string = "SELECT * FROM ajustador WHERE id_ajustador="+id_param;
        } else {
                sql_string = "SELECT * FROM ajustador";
        }
        let query = sql.query(sql_string, (err, results) => {
            if (err) {
                res.status(404).json({ "response": "Not Found" });
            } else {
                if (typeof results.length != undefined) {
                    res.status(200).json({ "response": results });
                } else {
                    res.status(404).json({ "response": "Not Found" });
                }
            }
        });
    }
});
// cambiar contrasena
app.put('/ajustador/:jwt/:id', (req, res) => {
    let token_jwt = req.params.jwt;
    if (!token_jwt) {
        res.status(403).json({ "response": "Forbidden" });
    } else {
        /**
          *  AQUI VA LA VERIFICACION TOKEN
          */
        let id_param = req.params.id;
        let contta = req.query.passwrd;
        if (!id_param) {
            res.status(404).json({ "response": "Not found" });
        } else {
            let sql_string = "";
            if (contta) {
                sql_string = "UPDATE vehiculo SET passwrd=" + contta +" WHERE id_vehiculo=" + id_param;
            }
            let query = sql.query(sql_string, (err, results) => {
                if (err) {
                    res.status(404).json({ "response": "Not found" });
                } else {
                    res.status(200).json({ "response": "Created" });
                }
            });
        }
    }
});

//add new ajustador 
// PROBADA
app.post('/ajustador/:jwt?', (req, res) => {
    let jwt = req.params.jwt;
    let sql_string = "INSERT INTO ajustador SET ?";
    if (!jwt) {
        res.status(403).json({ "response": "Forbidden" });
    }
    else {
        let data = {
            nombre: req.body.nombre,
            username: req.body.username,
            passwrd: req.body.passwrd
        };
        let query = sql.query(sql_string, data, (err, results) => {
            if (err) {
                res.status(404).json({ "response": "Not Found" });
            } else {
                res.status(201).json({ "response": "Created" });
            }
        });
    }
});


module.exports = app;
