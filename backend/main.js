'use strict';
// require express and bodyparser
const express = require('express');
const bodyParser = require("body-parser");

// Constants
const PORT = 80;
const HOST = '0.0.0.0';
//const sql = require("./app/db/db.js");
// App
const app = express();
// parse requests of content-type: application/json
app.use((req, res, next) => {
	res.header('Access-Control-Allow-Origin', '*');
	res.header('Access-Control-Allow-Headers', 'Authorization, X-API-KEY, Origin, X-Requested-With, Content-Type, Accept, Access-Control-Allow-Request-Method');
	res.header('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, DELETE');
	res.header('Allow', 'GET, POST, OPTIONS, PUT, DELETE');
	next();
});
app.use(bodyParser.json());
// parse requests of content-type: application/x-www-form-urlencoded
app.use(bodyParser.urlencoded({ extended: true }));
// import funciones de vehiculo;
app.use(require('./app/db/crud_vehiculo.js'));
// import funciones de foto;
app.use(require('./app/db/crud_foto.js'));
// import funciones de vehiculo;
app.use(require('./app/db/crud_ajustador.js'));
// import funciones de foto;
app.use(require('./app/db/login.js'));

app.listen(PORT, HOST);
console.log(`Running on http://${HOST}:${PORT}`);
module.exports = app;
