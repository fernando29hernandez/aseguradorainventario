#!/bin/bash

while :
do
  if ping -c 1 0.0.0.0:80 &> /dev/null
  then
    echo "Host is online"
    break
  fi
  sleep 5
done