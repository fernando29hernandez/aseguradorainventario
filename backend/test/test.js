// Import the dependencies for testing

const chai = require('chai');
const chaiHttp = require('chai-http');
//const app = require('../main.js');
// Configure chai
chai.use(chaiHttp);
var should = require('chai').should();
describe("App", () => {
    describe("Fotos", () => {
        describe("GET /", () => {
            // Test to get all fotos record
            it("Respuesta de obtener fotos", (done) => {
                chai.request('localhost:80')
                    .get('/Foto')
                    .end((err, res) => {
                        should.not.exist(err);
                        should.exist(res); 
                        res.should.have.status(403);
                        done();
                    });
            });
        });
        describe("POST /", () => {
            it("Respuesta de insertar foto", (done) => {
                chai.request('localhost:80')
                    .post('/Foto/prueba')
                    .end((err, res) => {
                        should.not.exist(err);
                        should.exist(res); 
                        res.should.have.status(404);
                        done();
                    });
            });
        });
    });
    describe("Vehiculos", () => {
        describe("GET /", () => {
            // Test to get all vehiculos record
            it("Respuesta de obtener fotos", (done) => {
                chai.request('localhost:80')
                    .get('/Vehiculo')
                    .end((err, res) => {
                        should.not.exist(err);
                        should.exist(res); 
                        res.should.have.status(403);
                        done();
                    });
            });
        });
        describe("POST /", () => {
            it("Respuesta de insertar foto", (done) => {
                chai.request('localhost:80')
                    .post('/Vehiculo')
                    //.send({id:0, country: "Croacia", year: 2017, days: 10})
                    .end((err, res) => {
                        should.not.exist(err);
                        should.exist(res); 
                        res.should.have.status(403);
                        done();
                    });
            });
        });
    });
    describe("Ajustadores", () => {
        describe("GET /", () => {
            // Test to get all ajustadores record
            it("Respuesta de obtener ajustadores", (done) => {
                chai.request('localhost:80')
                    .get('/ajustador/prueba')
                    .end((err, res) => {
                        should.not.exist(err);
                        should.exist(res); 
                        res.should.have.status(200);
                        done();
                    });
            });
        });
        describe("POST /", () => {
            it("Respuesta de almacenar ajustador", (done) => {
                chai.request('localhost:80')
                    .post('/ajustador')
                    .end((err, res) => {
                        should.not.exist(err);
                        should.exist(res); 
                        res.should.have.status(403);
                        done();
                    });
            });
        });
        describe("GET /", () => {
            it("Respuesta de prueba de login OK ", (done) => {
                chai.request('localhost:80')
                    .get('/login/prueba?username=admin&passwrd=1234')
                    .end((err, res) => {
                        should.not.exist(err);
                        should.exist(res); 
                        res.should.have.status(200);
                        done();
                    });
            });
        });
        describe("GET /", () => {
            it("Respuesta de prueba de login Not found", (done) => {
                chai.request('localhost:80')
                    .get('/login/prueba?username=admin&passwrd=123')
                    .end((err, res) => {
                        should.not.exist(err);
                        should.exist(res); 
                        res.should.have.status(404);
                        done();
                    });
            });
        });
    })
});
