
SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL,ALLOW_INVALID_DATES';


CREATE DATABASE IF NOT EXISTS inventario DEFAULT CHARACTER SET utf8 ;

USE inventario;
/* =============================================
 * =====TABLA Ajustador=========================
 * =============================================
 */
CREATE TABLE IF NOT EXISTS ajustador(
    id_ajustador INT UNSIGNED NOT NULL AUTO_INCREMENT,
    nombre VARCHAR(100),
    username VARCHAR(100),
    passwrd VARCHAR(100),
    PRIMARY KEY (`id_ajustador`)
)ENGINE = InnoDB;


/* =============================================
 * =====TABLA estado============================
 * =============================================
 */

CREATE TABLE IF NOT EXISTS estado(
    id INT UNSIGNED NOT NULL ,
    descripcion VARCHAR(100),
    PRIMARY KEY (`id`)
)ENGINE = InnoDB;

/* =============================================
 * =====TABLA Vehiculo==========================
 * =============================================
 */
CREATE TABLE IF NOT EXISTS vehiculo (
    id INT UNSIGNED NOT NULL AUTO_INCREMENT,
    estado INT UNSIGNED,
    tipo VARCHAR(100),
    marca VARCHAR(100),
    linea VARCHAR(100),
    modelo VARCHAR(100),
    placa VARCHAR(100),
    color VARCHAR(100),
    numero_chasis VARCHAR(100),
    numero_motor VARCHAR(100),
    ajustador INT UNSIGNED,
    precio_base DECIMAL(18,2),
    precio_minimo DECIMAL(18,2),
    arranca BOOLEAN,
    camina BOOLEAN,
    falla_mecanica BOOLEAN,
    garantia_inspeccion BOOLEAN,
    inundado BOOLEAN,
    colision BOOLEAN,
    fecha_hora timestamp  NOT NULL DEFAULT CURRENT_TIMESTAMP,
    observacion VARCHAR(500),
    PRIMARY KEY (`id`),
    FOREIGN KEY (ajustador) REFERENCES ajustador(id_ajustador)
)ENGINE = InnoDB;

/* =============================================
 * =====TABLA Fotos=============================
 * =============================================
 */
CREATE TABLE IF NOT EXISTS foto_vehiculo(
    id INT UNSIGNED NOT NULL AUTO_INCREMENT,
    url VARCHAR(100) NOT NULL,
    externa BOOLEAN NOT NULL,
    fk_vehiculo INT UNSIGNED NOT NULL,
    PRIMARY KEY (`id`),
    FOREIGN KEY (fk_vehiculo) REFERENCES vehiculo(id)
)ENGINE = InnoDB;
/* =============================================
 * =====TABLA puja=============================
 * =============================================
 */
CREATE TABLE IF NOT EXISTS puja(
    id INT UNSIGNED NOT NULL AUTO_INCREMENT,
    id_afiliado int UNSIGNED,
    id_vehiculo int UNSIGNED,
    monto DECIMAL(18,2),
    PRIMARY KEY (`id`),
    FOREIGN KEY (id_vehiculo) REFERENCES vehiculo(id)
)ENGINE = InnoDB;

insert into ajustador(nombre,username,passwrd) values("Fernando","admin","1234");
insert into ajustador(nombre,username,passwrd) values("Antonio","admin2","1234");
insert into estado(id,descripcion) values(1,"En transito");
insert into estado(id,descripcion) values(2,"En almacenaje");
insert into estado(id,descripcion) values(3,"Subastable");
insert into estado(id,descripcion) values(4,"Adjudicado");
insert into estado(id,descripcion) values(5,"Vendido");
/*
insert into 
vehiculo(
    estado,
    tipo,
    marca,
    linea,
    modelo,
    placa,
    color,
    numero_chasis,
    numero_motor,
    ajustador,
    precio_base,
    precio_minimo,
    arranca,
    camina,
    falla_mecanica,
    garantia_inspeccion,
    inundado,
    colision,
    observacion) 
    values(1,"sedan","toyota","yaris","2012","480-DRZ","negro",
    "numero de prueba chasis","numero de prueba motor",1,90000.00,100000.00,
    true,true,false,true,false,false,"carro no 1");
insert into 
vehiculo(
    estado,
    tipo,
    marca,
    linea,
    modelo,
    placa,
    color,
    numero_chasis,
    numero_motor,
    ajustador,
    precio_base,
    precio_minimo,
    arranca,
    camina,
    falla_mecanica,
    garantia_inspeccion,
    inundado,
    colision,
    observacion) 
    values(1,"sedan","honda","civic","2011","999-DBZ","gris",
    "numero de prueba chasis","numero de prueba motor",1,90000.00,100000.00,
    true,true,false,true,false,false,"carro no 1");
/*
insert into foto_vehiculo(url,externa,fk_vehiculo) values("ruta_de_prueba",false,1);
insert into foto_vehiculo(url,externa,fk_vehiculo) values("ruta_de_prueba2",false,1);
insert into foto_vehiculo(url,externa,fk_vehiculo) values("ruta_de_prueba3",true,1);
insert into foto_vehiculo(url,externa,fk_vehiculo) values("ruta_de_prueba4",true,1);
insert into foto_vehiculo(url,externa,fk_vehiculo) values("ruta_de_prueba5",false,1);
insert into foto_vehiculo(url,externa,fk_vehiculo) values("ruta_de_prueba6",false,1);
insert into foto_vehiculo(url,externa,fk_vehiculo) values("ruta_de_prueba7",false,1);
insert into foto_vehiculo(url,externa,fk_vehiculo) values("ruta_de_prueba8",false,1);
*/
SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;


